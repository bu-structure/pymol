#ifndef _PyMOL_VERSION
#define _PyMOL_VERSION "1.8.5.0"
#endif

#ifndef _PyMOL_VERSION_double
#define _PyMOL_VERSION_double 1.850
#endif


/* for session file compatibility */

#ifndef _PyMOL_VERSION_int
#define _PyMOL_VERSION_int 1850
#endif
